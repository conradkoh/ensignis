# Getting Started

## Building the container

The following command starts the docker container

```shell
npm run docker:build
```

You can visit the application at http://localhost:3000
