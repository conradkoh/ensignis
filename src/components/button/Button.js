import * as React from 'react';
import MUIButton from '@material-ui/core/Button';
/**
 * @param {ButtonProps} props
 */
function Button(props) {
    const { label, onClick, color, variant = 'contained', style, disabled } = props;
    return (
        <MUIButton style={style} variant={variant} color={color || 'primary'} onClick={() => onClick()} disabled={disabled}>
            {label}
        </MUIButton>
    );
}
export default Button;
/**
 * @typedef ButtonProps
 * @property {'primary' | 'secondary'} [color]
 * @property {string} [label]
 * @property {function} [onClick]
 * @property {'text' | 'outlined' | 'contained'} [variant]
 * @property {React.CSSProperties} [style]
 * @property {boolean} [disabled]
 */
