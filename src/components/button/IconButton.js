import * as React from 'react';
import MUIIconButton from '@material-ui/core/IconButton';
/**
 * @param {IconButtonProps} props
 */
function IconButton(props) {
    const { icon, onClick = () => {} } = props;
    return <MUIIconButton onClick={(e) => onClick()}>{icon}</MUIIconButton>;
}
export default IconButton;
/**
 * @typedef IconButtonProps
 * @property {*} [icon]
 * @property {function} [onClick]
 */
