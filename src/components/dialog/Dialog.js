import * as React from 'react';
import MUIDialog from '@material-ui/core/Dialog';
/**
 * @param {DialogProps} props
 */
function Dialog(props) {
    const { children, open } = props;
    return <MUIDialog open={open}>{children}</MUIDialog>;
}
export default Dialog;
/**
 * @typedef DialogProps
 * @property {boolean} open
 * @property {any} [children]
 */

// import * as React from 'react';
// import MUIDialog from '@material-ui/core/Dialog';
// /**
//  * @extends {React.Component<DialogProps>}
//  */
// class Dialog extends React.Component {
//     constructor(props) {
//         super(props);
//         this.state = {};
//     }
//     render() {
//         const { children, open } = this.props;
//         return <MUIDialog open={open}>{children}</MUIDialog>;
//     }
// }
// export default Dialog;
// /**
//  * @typedef DialogProps
//  * @property {boolean} open
//  * @property {any} [children]
//  */
