import MUIDeleteIcon from '@material-ui/icons/Delete';
import * as React from 'react';
/**
 * @param {DeleteIconProps} props
 */
function DeleteIcon(props) {
    return <MUIDeleteIcon htmlColor="red" />;
}
export default DeleteIcon;
/**
 * @typedef {*} DeleteIconProps
 */
