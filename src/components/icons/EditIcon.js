import MUIEditIcon from '@material-ui/icons/Edit';
import * as React from 'react';
/**
 * @param {EditIconProps} props
 */
function EditIcon(props) {
    return <MUIEditIcon htmlColor="blue" />;
}
export default EditIcon;
/**
 * @typedef {*} EditIconProps
 */
