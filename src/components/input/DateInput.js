import * as React from 'react';
import MUITextField from '@material-ui/core/TextField';
/**
 * @param {DateInputProps} props
 */
function DateInput(props) {
    const { value, label, error, onChange } = props;
    return (
        <MUITextField
            value={value}
            type="date"
            fullWidth={true}
            label={label}
            onChange={(e) => onChange(e.target.value)}
            InputLabelProps={{
                shrink: true,
            }}
            helperText={error}
            error={error ? true : false}
        />
    );
}
export default DateInput;
/**
 * @typedef DateInputProps
 * @property {string} value
 * @property {string} [label]
 * @property {string} [error]
 * @property {function(string)} [onChange]
 */
