import * as React from 'react';
import MUITextField from '@material-ui/core/TextField';
/**
 * @param {InputProps} props
 */
function Input(props) {
    const { value, label, error, onChange } = props;
    return (
        <MUITextField
            value={value}
            fullWidth={true}
            label={label}
            onChange={(e) => onChange(e.target.value)}
            helperText={error}
            error={error ? true : false}
        />
    );
}
export default Input;
/**
 * @typedef InputProps
 * @property {string} [value]
 * @property {string} [label]
 * @property {string} [error]
 * @property {function(string)} [onChange]
 */
