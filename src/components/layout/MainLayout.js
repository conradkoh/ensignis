import * as React from 'react';
/**
 * @param {MainLayoutProps} props
 */
function MainLayout(props) {
    return <div>{props.children}</div>;
}
export default MainLayout;
/**
 * @typedef MainLayoutProps
 * @property {*} [children]
 */
