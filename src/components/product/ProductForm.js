import * as React from 'react';
import Dialog from '../../components/dialog/Dialog';
import Button from '../../components/button/Button';
import Input from '../../components/input/Input';
import DateInput from '../../components/input/DateInput';
import { formatDateForInput } from '../../helpers/date';
/**
 * @param {ProductFormProps} props
 */
function ProductForm(props) {
    const { isDialogOpen, formData, formErrors, onSave, onFieldUpdate, resetFormData, setDialogOpen, isFormValid } = props;
    const { product_id, date, price, quantity } = formData;
    const formMode = product_id ? 'update' : 'create';
    return (
        <div>
            {/* Create Product Dialog */}
            <Dialog open={isDialogOpen}>
                <div className="d-flex flex-column">
                    <div className="p-3">
                        <h4>Create Product Form</h4>
                    </div>
                    {/* Inner Form */}
                    <div className="d-flex flex-column flex-grow-1 p-3">
                        <div className="d-flex flex-grow-1 align-items-center w-100 h-100">
                            <div className="w-100">
                                <DateInput
                                    value={formatDateForInput(date)}
                                    error={formErrors.date}
                                    label="Date"
                                    onChange={(date) => onFieldUpdate('date', new Date(date))}
                                />
                                <br />
                                <Input
                                    value={`${price}`}
                                    error={formErrors.price}
                                    label="Price"
                                    onChange={(price) => onFieldUpdate('price', parseFloat(price))}
                                />
                                <br />
                                <Input
                                    value={`${quantity}`}
                                    error={formErrors.quantity}
                                    label="Quantity"
                                    onChange={(quantity) => onFieldUpdate('quantity', parseFloat(quantity))}
                                />
                            </div>
                        </div>
                    </div>
                    {/* Action Buttons */}
                    <div className="d-flex flex-row p-3">
                        <div className="flex-grow-1" />
                        <Button
                            variant="outlined"
                            label="Close"
                            onClick={() => {
                                resetFormData();
                                setDialogOpen(!isDialogOpen);
                            }}
                        />
                        <Button
                            label={formMode === 'create' ? 'Create' : 'Update'}
                            disabled={!isFormValid}
                            onClick={() => {
                                onSave(formMode, {
                                    product_id: formData.product_id,
                                    date: new Date(formData.date),
                                    price: formData.price,
                                    quantity: formData.quantity,
                                });
                                resetFormData();
                                setDialogOpen(false);
                            }}
                        />
                    </div>
                </div>
            </Dialog>
        </div>
    );
}
export default ProductForm;
/**
 * @typedef ProductFormProps
 * @property {*} formErrors
 * @property {Product} formData
 * @property {boolean} isDialogOpen
 * @property {function} [onSave]
 * @property {function(string, *)} onFieldUpdate
 * @property {function} resetFormData
 * @property {function} setDialogOpen
 * @property {boolean} isFormValid
 */

/**
 * @typedef Product
 * @property {string} [product_id]
 * @property {*} [date]
 * @property {*} [price]
 * @property {*} [quantity]
 */
