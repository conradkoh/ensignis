import * as React from 'react';
import IconButton from '../button/IconButton';
import EditIcon from '../icons/EditIcon';
import DeleteIcon from '../icons/DeleteIcon';

/**
 *
 * @param {ProductListProps} props
 */
export function ProductList(props) {
    let { products, onProductEdit, onProductDelete } = props;
    /** @type {React.CSSProperties} */
    const thStyle = { textAlign: 'center' };
    return (
        <table className="w-100">
            <thead>
                <tr>
                    <th style={thStyle}>Product Id</th>
                    <th style={thStyle}>Date</th>
                    <th style={thStyle}>Price</th>
                    <th style={thStyle}>Quantity</th>
                    <th style={thStyle}>Edit</th>
                    <th style={thStyle}>Delete</th>
                </tr>
            </thead>
            <tbody>
                {products.map((p, idx) => (
                    <Product key={idx} product={p} onEdit={onProductEdit} onDelete={onProductDelete} />
                ))}
            </tbody>
        </table>
    );
}
/**
 * @typedef ProductListProps
 * @property {Product[]} products
 * @property {function(Product)} onProductEdit
 * @property {function(Product)} onProductDelete
 */

const iconTdStyle = {
    width: '45px',
};
/**
 *
 * @param {{product: Product, onEdit: Function, onDelete: function }} props
 */
export function Product(props) {
    let { product, onEdit, onDelete } = props;
    const { product_id, date, price, quantity } = product;
    if (!date.toDateString) {
        console.log(typeof date, date);
    }
    return (
        <tr>
            <td>
                <span>{product_id}</span>
            </td>
            <td>
                <span>{date.toDateString()}</span>
            </td>
            <td>
                <span>{price}</span>
            </td>
            <td>
                <span>{quantity}</span>
            </td>
            <td style={iconTdStyle}>
                <IconButton onClick={() => onEdit(product)} icon={<EditIcon />} />
            </td>
            <td style={iconTdStyle}>
                <IconButton onClick={() => onDelete(product)} icon={<DeleteIcon />} />
            </td>
        </tr>
    );
}

/**
 * @typedef {import('../../store/actions/products').Product} Product
 */
