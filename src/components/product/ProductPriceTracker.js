import * as React from 'react';
import LineChartSingle from '../../lib/components/chart/line/LineChartSingle';
const PERIOD_LENGTH_DAYS = 7;
/**
 * @param {ProductPriceTrackerProps} props
 */
function ProductPriceTracker(props) {
    let { products } = props;
    let product_data_aggr = formatProductsData(products, PERIOD_LENGTH_DAYS);
    let { aggrgated_average_price, aggregated_product_quantity } = product_data_aggr;
    /** @type {React.CSSProperties} */
    let chartWrapperStyle = { height: '200px', width: '100%' };
    return (
        <div>
            <div style={chartWrapperStyle}>
                <LineChartSingle
                    xLabel="Time"
                    yLabel="Price"
                    data={[
                        {
                            id: 'Price',
                            data: product_data_aggr.price_data,
                        },
                    ]}
                />
            </div>
            <div style={chartWrapperStyle}>
                <LineChartSingle
                    xLabel="Time"
                    yLabel="Quantity"
                    data={[
                        {
                            id: 'Quantity',
                            data: product_data_aggr.quantity_data,
                        },
                    ]}
                />
            </div>
            <div>
                <span>
                    <b>Period Length (Days):</b> {PERIOD_LENGTH_DAYS}
                </span>
                <span className="ml-4">
                    <b>Number of Products:</b> {products.length}
                </span>
                <span className="ml-4">
                    <b>Aggregated Average Price:</b> {aggrgated_average_price}
                </span>
                <span className="ml-4">
                    <b>Aggregated Product Quantity:</b> {aggregated_product_quantity}
                </span>
            </div>
        </div>
    );
}
export default ProductPriceTracker;
/**
 * @typedef ProductPriceTrackerProps
 * @property {Product[]} products
 */
/**
 * @typedef {import('../../store/actions/products').Product} Product
 */

/**
 * @param {Product[]} products
 */
function formatProductsData(products, days) {
    const stats = {
        aggregated_product_quantity: 0,
        aggrgated_average_price: 0,
    };
    //Enforce sort order in the framing of the data
    products = products.sort((a, b) => {
        if (a.date > b.date) {
            return 1;
        } else if (a.date < b.date) {
            return -1;
        } else {
            return -0;
        }
    });

    let average_prices = [];
    let average_units = [];
    let first_product = products[0];
    let current_timeframe_start = first_product ? first_product.date.getTime() : new Date().getTime();
    let current_timeframe_end = current_timeframe_start + days * 86400000;
    let current_timeframe_products = 0;
    let current_timeframe_average_price = 0;
    for (let product of products) {
        //When frame limit is hit, insert new data point
        if (product.date.getTime() > current_timeframe_end) {
            average_prices.push({
                timeframe_start: current_timeframe_start,
                timeframe_end: current_timeframe_end,
                average_price: current_timeframe_average_price,
            });
            average_units.push({
                timeframe_start: current_timeframe_start,
                timeframe_end: current_timeframe_end,
                average_units: current_timeframe_products / days, //Average products per day is the sum of total products moved over the time frame which we are computing the stats for
            });

            //Reset iteration when frame limit is hit
            current_timeframe_start = current_timeframe_end;
            current_timeframe_end = current_timeframe_start + days * 86400000;
            current_timeframe_products = 0;
            current_timeframe_average_price = 0;
        }
        //Compute the price and total number of products moved in the current timeframe
        current_timeframe_average_price =
            (current_timeframe_products / (current_timeframe_products + product.quantity)) * current_timeframe_average_price +
            (product.quantity / (current_timeframe_products + product.quantity)) * product.price;
        current_timeframe_products += product.quantity;

        //Compute overall states
        stats.aggrgated_average_price =
            (stats.aggregated_product_quantity / (stats.aggregated_product_quantity + product.quantity)) * stats.aggrgated_average_price +
            (product.quantity / (stats.aggregated_product_quantity + product.quantity)) * product.price;

        stats.aggregated_product_quantity += product.quantity;
    }

    //Insert any remaining data points before the frame limit was hit
    average_prices.push({
        timeframe_start: current_timeframe_start,
        timeframe_end: current_timeframe_end,
        average_price: current_timeframe_average_price,
    });

    average_units.push({
        timeframe_start: current_timeframe_start,
        timeframe_end: current_timeframe_end,
        average_units: (current_timeframe_products / days).toFixed(2),
    });
    //Format
    let price_data = average_prices.map((p) => ({
        x: `${new Date(p.timeframe_start).toDateString()} - ${new Date(p.timeframe_end).toDateString()}`,
        y: p.average_price,
    }));

    let quantity_data = average_units.map((p) => ({
        x: `${new Date(p.timeframe_start).toDateString()} - ${new Date(p.timeframe_end).toDateString()}`,
        y: p.average_units,
    }));
    return {
        price_data,
        quantity_data,
        ...stats,
    };
}
