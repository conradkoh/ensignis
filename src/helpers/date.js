/**
 * @param {Date} date
 */
export function formatDateForInput(date) {
    if (!date) {
        return null;
    }
    const mm_raw = date.getMonth() + 1;
    const dd_raw = date.getDate();
    const yyyy = date.getFullYear();
    const mm = mm_raw < 10 ? `0${mm_raw}` : `${mm_raw}`;
    const dd = dd_raw < 10 ? `0${dd_raw}` : `${dd_raw}`;
    return `${yyyy}-${mm}-${dd}`;
}
