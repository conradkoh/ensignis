import * as uuid from 'uuid';

/**
 * @returns {Product[]}
 */
export function getTestDataSet() {
    let TIMESTAMP_SEED = 1587054870212;
    let ONE_DAY = 86400000 * 3; //24 * 60 * 60 * 1000
    let FINAL_PRICE = 10000;
    let STEP = 1000;
    let ENTRY_COUNT = 10;
    let entries = [];
    for (let i = 0; i < ENTRY_COUNT; i++) {
        entries.push({
            product_id: uuid.v4(),
            date: new Date(TIMESTAMP_SEED - i * ONE_DAY),
            price: FINAL_PRICE - i * STEP,
            quantity: 50,
        });
    }
    return entries;
}
/**
 * @typedef {import('../store/reducers/products').Product} Product
 */
