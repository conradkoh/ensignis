import * as React from 'react';
export function withForm(Component) {
    let formData = {};
    let errors = {};
    let validations = {};
    return function (props) {
        return <Component form={{ formData, errors, validations }} {...props} />;
    };
}
export default withForm;
