import * as React from 'react';
import Card from '@material-ui/core/Card/Card';

/**
 * @param {OutlinedCardProps} props
 */
function OutlinedCard(props) {
    let { className, style } = props;
    return (
        <Card className={className} style={style} variant="outlined">
            {props.children}
        </Card>
    );
}
export default OutlinedCard;
/**
 * @typedef OutlinedCardProps
 * @property {string} [className]
 * @property {React.CSSProperties} [style]
 * @property {*} children
 */
