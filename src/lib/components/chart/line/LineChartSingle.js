import * as React from 'react';
import { ResponsiveLine } from '@nivo/line';
/**
 * @param {LineChartSingleProps} props
 */
function LineChartSingle(props) {
    let {
        data,
        xLabel,
        xLabelOffset = 36,
        yLabel,
        yLabelOffset = -40,
        style = {},
        className,
    } = props;
    let maxChar = 33;
    return (
        <div style={{ height: '100%', width: '100%', ...style }} className={className}>
            <ResponsiveLine
                data={data}
                margin={{ top: 50, right: maxChar * 3, bottom: 50, left: maxChar * 3 }}
                xScale={{ type: 'point' }}
                yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                axisTop={null}
                axisRight={null}
                axisBottom={{
                    orient: 'bottom',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: xLabel,
                    legendOffset: xLabelOffset,
                    legendPosition: 'middle',
                }}
                axisLeft={{
                    orient: 'left',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: yLabel,
                    legendOffset: yLabelOffset,
                    legendPosition: 'middle',
                }}
                colors={{ scheme: 'nivo' }}
                pointSize={10}
                pointColor={{ theme: 'background' }}
                pointBorderWidth={2}
                pointBorderColor={{ from: 'serieColor' }}
                pointLabel="y"
                pointLabelYOffset={-12}
                useMesh={true}
                legends={[
                    {
                        anchor: 'bottom-right',
                        direction: 'column',
                        justify: false,
                        translateX: 100,
                        translateY: 0,
                        itemsSpacing: 0,
                        itemDirection: 'left-to-right',
                        itemWidth: 80,
                        itemHeight: 20,
                        itemOpacity: 0.75,
                        symbolSize: 12,
                        symbolShape: 'circle',
                        symbolBorderColor: 'rgba(0, 0, 0, .5)',
                        effects: [
                            {
                                on: 'hover',
                                style: {
                                    itemBackground: 'rgba(0, 0, 0, .03)',
                                    itemOpacity: 1,
                                },
                            },
                        ],
                    },
                ]}
            />
        </div>
    );
}
export default LineChartSingle;
/**
 * @typedef LineChartSingleProps
 * @property {string} [xLabel]
 * @property {string} [yLabel]
 * @property {number} [xLabelOffset]
 * @property {number} [yLabelOffset]
 * @property {React.CSSProperties} [style]
 * @property {string} [className]
 * @property {*} data
 */
