import * as React from 'react';
import { connect } from 'react-redux';
import MainLayout from '../components/layout/MainLayout';
import { ProductList } from '../components/product/ProductList';
import ProductPriceTracker from '../components/product/ProductPriceTracker';
import Button from '../components/button/Button';
import { addProduct, updateProduct, deleteProduct, loadProducts } from '../store/actions/products';
import ProductForm from '../components/product/ProductForm';
import { getTestDataSet } from '../helpers/test';

const FORM_VALIDATIONS = {
    date: (value) => {
        if (!value) {
            return 'Date cannot be empty';
        }
        return '';
    },
    price: (value) => {
        if (!value) {
            return 'Invalid price';
        }
        return '';
    },
    quantity: (value) => {
        if (!value) {
            return 'Invalid quantity';
        }
        return '';
    },
};

export function Products(props) {
    const { addProduct, updateProduct, deleteProduct, loadProducts } = props;
    const [isDialogOpen, setDialogOpen] = React.useState(false);
    const [formData, setFormData] = React.useState({
        date: null,
        price: 0,
        quantity: 0,
    });
    const [formErrors, setFormErrors] = React.useState({
        date: '',
        price: '',
        quantity: '',
    });
    const [isFormValid, setFormVaild] = React.useState(false);

    React.useEffect(() => {
        let isFormValid = true;
        for (let fieldName in FORM_VALIDATIONS) {
            let validation = FORM_VALIDATIONS[fieldName];
            let fieldValue = formData[fieldName];
            if (validation) {
                let error = validation(fieldValue);
                if (error) {
                    isFormValid = false;
                    break;
                }
            }
        }
        setFormVaild(isFormValid);
    }, [formData, formErrors]);
    React.useEffect(() => {
        for (let fieldName in FORM_VALIDATIONS) {
            let validation = FORM_VALIDATIONS[fieldName];
            let fieldValue = formData[fieldName];
            if (validation) {
                let error = validation(fieldValue);
                formErrors[fieldName] = error;
            }
        }
        setFormErrors(formErrors);
    }, [formData]);

    const onFormFieldUpdate = (fieldName, fieldValue) => {
        let validation = FORM_VALIDATIONS[fieldName];
        if (validation) {
            //Perform the field validation if it is available
            formErrors[fieldName] = validation(fieldValue);
            setFormErrors(formErrors);
        }
        formData[fieldName] = fieldValue;
        setFormData({
            ...formData,
        });
    };
    const resetFormData = () => {
        setFormData({
            date: null,
            price: 0,
            quantity: 0,
        });
    };

    const editProductInForm = (formData) => {
        setFormData(formData);
        setDialogOpen(true);
    };

    const deleteProductInForm = (formData) => {
        deleteProduct(formData.product_id);
    };
    const btnStyle = {
        minHeight: '70px',
        width: '100%',
    };
    return (
        <MainLayout>
            <div>Products</div>
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                <ProductPriceTracker products={props.products} />
                <br />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-10">
                            <ProductList
                                products={props.products}
                                onProductEdit={(product) => editProductInForm(product)}
                                onProductDelete={(product) => deleteProductInForm(product)}
                            />
                        </div>
                        <div className="col-2">
                            <Button style={btnStyle} label="Create product record" onClick={() => setDialogOpen(!isDialogOpen)} />
                            <div className="p-2" />
                            <Button style={btnStyle} label="Generate Test Data" onClick={() => loadProducts(getTestDataSet())} />
                            <div className="p-2" />
                            <Button style={btnStyle} label="Reset Data" onClick={() => loadProducts([])} />
                        </div>
                    </div>
                </div>
            </div>

            <ProductForm
                isDialogOpen={isDialogOpen}
                formData={formData}
                formErrors={formErrors}
                onSave={(formMode, formData) => {
                    switch (formMode) {
                        case 'create': {
                            addProduct(formData);
                            break;
                        }
                        case 'update': {
                            updateProduct(formData);
                            break;
                        }
                        default:
                    }
                }}
                onFieldUpdate={onFormFieldUpdate}
                resetFormData={resetFormData}
                setDialogOpen={setDialogOpen}
                isFormValid={isFormValid} //Controls whether the submit button can be pressed
            />
        </MainLayout>
    );
}
const mapStateToProps = (state) => ({
    products: state.products,
});

const mapDisppatchToProps = {
    addProduct: addProduct,
    updateProduct: updateProduct,
    deleteProduct: deleteProduct,
    loadProducts: loadProducts,
};
export default connect(mapStateToProps, mapDisppatchToProps)(Products);
