import * as React from 'react';
import { connect } from 'react-redux';
import MainLayout from '../components/layout/MainLayout';
import { ProductList } from '../components/product/ProductList';
import ProductPriceTracker from '../components/product/ProductPriceTracker';
import Button from '../components/button/Button';
import { addProduct, updateProduct, deleteProduct, loadProducts } from '../store/actions/products';
import ProductForm from '../components/product/ProductForm';
import { getTestDataSet } from '../helpers/test';
export function Products(props) {
    const { addProduct, updateProduct, deleteProduct, loadProducts } = props;
    const [isDialogOpen, setDialogOpen] = React.useState(false);
    const [formData, setFormData] = React.useState({
        date: null,
        price: 0,
        quantity: 0,
    });
    const resetFormData = () => {
        setFormData({
            date: null,
            price: 0,
            quantity: 0,
        });
    };

    const editProductInForm = (formData) => {
        setFormData(formData);
        setDialogOpen(true);
    };

    const deleteProductInForm = (formData) => {
        deleteProduct(formData.product_id);
    };
    const btnStyle = {
        minHeight: '70px',
        width: '100%',
    };
    return (
        <MainLayout>
            <div>Products</div>
            <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
                <ProductPriceTracker products={props.products} />
                <br />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-10">
                            <ProductList
                                products={props.products}
                                onProductEdit={(product) => editProductInForm(product)}
                                onProductDelete={(product) => deleteProductInForm(product)}
                            />
                        </div>
                        <div className="col-2">
                            <Button style={btnStyle} label="Create product record" onClick={() => setDialogOpen(!isDialogOpen)} />
                            <div className="p-2" />
                            <Button style={btnStyle} label="Generate Test Data" onClick={() => loadProducts(getTestDataSet())} />
                            <div className="p-2" />
                            <Button style={btnStyle} label="Reset Data" onClick={() => loadProducts([])} />
                        </div>
                    </div>
                </div>
            </div>

            <ProductForm
                isDialogOpen={isDialogOpen}
                formData={formData}
                onSave={(formMode, formData) => {
                    switch (formMode) {
                        case 'create': {
                            addProduct(formData);
                            break;
                        }
                        case 'update': {
                            updateProduct(formData);
                            break;
                        }
                        default:
                    }
                }}
                setFormData={setFormData}
                resetFormData={resetFormData}
                setDialogOpen={setDialogOpen}
            />
        </MainLayout>
    );
}
const mapStateToProps = (state) => ({
    products: state.products,
});

const mapDisppatchToProps = {
    addProduct: addProduct,
    updateProduct: updateProduct,
    deleteProduct: deleteProduct,
    loadProducts: loadProducts,
};
export default connect(mapStateToProps, mapDisppatchToProps)(Products);
