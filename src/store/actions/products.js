export const ADD_PRODUCT = 'ADD_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const LOAD_PRODUCTS = 'LOAD_PRODUCTS';
/**
 * @param {Product} product
 */
export const addProduct = (product) => ({
    type: ADD_PRODUCT,
    product,
});

/**
 * @param {Product} product
 */
export const updateProduct = (product) => ({
    type: UPDATE_PRODUCT,
    product,
});

/**
 * @param {string} product_id
 */
export const deleteProduct = (product_id) => ({
    type: DELETE_PRODUCT,
    product_id,
});

export const loadProducts = (products) => ({
    type: LOAD_PRODUCTS,
    products,
});
/**
 * @typedef Product
 * @property {string} product_id
 * @property {Date} date
 * @property {number} quantity
 * @property {number} price
 */
