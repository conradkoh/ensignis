import { ADD_PRODUCT, UPDATE_PRODUCT, DELETE_PRODUCT, LOAD_PRODUCTS } from '../actions/products';
import * as uuid from 'uuid';
/**
 *
 * @param {Product[]} state
 * @param {*} action
 */
const products = (state = [], action) => {
    switch (action.type) {
        case ADD_PRODUCT: {
            state = [...state, { ...action.product, product_id: uuid.v4() }];
            return state;
        }
        case UPDATE_PRODUCT: {
            let found = -1;
            for (let idx = 0; idx < state.length; idx++) {
                let product = state[idx];
                if (product.product_id === action.product.product_id) {
                    found = idx;
                    break;
                }
            }
            if (found >= 0) {
                let product = state[found];
                product = { ...product, ...action.product };
                state[found] = product;
            }
            return state;
        }
        case DELETE_PRODUCT: {
            let found = -1;
            for (let idx = 0; idx < state.length; idx++) {
                let product = state[idx];
                if (product.product_id === action.product_id) {
                    found = idx;
                    break;
                }
            }
            if (found >= 0) {
                state.splice(found, 1);
            }
            return [...state];
        }
        case LOAD_PRODUCTS: {
            return action.products;
        }
        default: {
            return state;
        }
    }
};
export default products;

/**
 * @typedef {import('../actions/products').Product} Product
 */
